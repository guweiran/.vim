" Enables smart pariting for pear-tree
" Smart pairing means the plugin will try to balance parentheses
let g:pear_tree_smart_openers=1
let g:pear_tree_smart_closers=1
let g:pear_tree_smart_backspace=1

nnoremap <silent> <leader>gf :Files<CR>
nnoremap <silent> <leader>gb :Buffers<CR>
nnoremap <silent> <leader>gl :Lines<CR>
nnoremap <silent> <leader>gm :Marks<CR>

call plug#begin()
	Plug 'tpope/vim-surround'	" Handles surrounding characters (eg brackets)
	Plug 'tpope/vim-repeat'	" Allows repeating commands for vim-surround
	Plug 'tpope/vim-commentary'	" Easier commenting
	Plug 'tpope/vim-sleuth'	" Autodetects tabs

	Plug 'airblade/vim-gitgutter'	" Shows git information in the gutter
	Plug 'tmsvg/pear-tree'	" Automatically pairs brackets
	Plug 'vim-scripts/argtextobj.vim'	" Adds function arg text objects
	Plug 'kana/vim-textobj-user'	" Adds custom text object support
	Plug 'kana/vim-textobj-indent'	"Adds text objects for indented blocks

	if has('python3')
		Plug 'SirVer/ultisnips'	" Snippets
		Plug 'honza/vim-snippets'	" Snippets
	endif

	Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }	" Fuzzy finder
	Plug 'junegunn/fzf.vim'	" Default bindings for fzf
	Plug 'w0rp/ale'	" Linting and LSP

call plug#end()
