" Behaviour
set backspace=indent,eol,start	" Good backspace behaviour
set lazyredraw	" Makes vim not redraw as often

set splitright	" Split vim windows to the right
set splitbelow	" Split vim windows below

set mouse=a	" Enable mouse support in Vim
set encoding=utf8	" Sets internal encoding to UTF-8

set wildmenu	" Shows an autocomplete menu for vim commands
set wildmode=longest,list	" Complete longest common string, then list alternatives.

set formatoptions+=r,o,j	" Add comment leader when adding a new line, and when joining, remove the comment leader
set nojoinspaces	" Only add 1 space after punctuation when joining

" File
set hidden	" Allow unsaved hidden buffers
filetype plugin indent on " Enables filetype detection on

" Interface
set number " Use line numbers
set linebreak	" Break lines at word (requires Wrap lines)
set wrap	" Wraps long lines
set showbreak=+++	" Wrap-broken line prefix
set linebreak	" Wrap only on whole words
set breakindent	" Wrapped indent follows indentation

set title	" Shows file being edited in titlebar
if has('macunix') " Set background colour according to the terminal background
	if system("defaults read -g AppleInterfaceStyle") =~ '^Dark'
		set background=dark
	else
		set background=light
	endif
else
	set background=dark
endif
set background=dark	" Uses lighter colours
set showmatch	" Highlight matching brace
set showcmd	" Shows vim commands in lower right corner
set scrolloff=5	" Keeps x lines above and below cursor
syntax enable	" Enables syntax highlighting

set ruler	" Show row and column ruler information
set list	" Shows hidden chars
set listchars=tab:→\ ,trail:•	" Show tab and trailing characters
set colorcolumn=80,120	" Show colour columns at 80 and 120 characters

set belloff=all	" Turns off the error bell for all events

if has ('macunix') " Set cursor shape in different modes
	let &t_SI.="\e[5 q" "SI = INSERT mode
	let &t_SR.="\e[4 q" "SR = REPLACE mode
	let &t_EI.="\e[1 q" "EI = NORMAL mode (ELSE)
endif

" Search
set hlsearch	" Highlight all search results
set smartcase	" Enable smart-case search
set ignorecase	" Ignore case when searching
set incsearch	" Searches for strings incrementally

" Indentation
set autoindent	" Auto-indent new lines
set shiftwidth=4	" Number of auto-indent spaces
set smartindent	" Smart autoindenting for new lines based on C rules
set smarttab	" Tab inserts spaces according to shiftwidth
set tabstop=4	" Sets tabs to 4 spaces visually
set softtabstop=4	" Number of spaces per tab

" Code folding
set foldlevel=99	" Start the code unfolded
set foldmethod=indent	" Folds based on indents

let plug_dir = split(&rtp, ',')[0]
if !filereadable(plug_dir.'/autoload/plug.vim')
	" Download plug.vim if unreadable
	if has('unix') " macOS or Linux
		silent execute '!curl -fLo '.plug_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
	else " Windows
		silent execute '!iwr -useb https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim |` ni '.plug_dir.'/autoload/plug.vim -Force'
	endif
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

if filereadable(plug_dir.'/autoload/plug.vim')
	" Only load the plugins if plug.vim is available
	exec 'source '.plug_dir.'/plugins.vim'
endif
